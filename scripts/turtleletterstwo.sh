#!/usr/bin/bash

rosservice call turtle1/set_pen 255 0 0 4 1

rosservice call turtle1/teleport_absolute 1.0 1.0 1.57

rosservice call turtle1/set_pen 255 0 0 4 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [3.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [4.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [0.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [4.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [3.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '

rosservice call /spawn 9.0 1.0 1.57 turtle2 

rosservice call turtle2/set_pen 255 180 204 3 0 
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- ' [1.8 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- ' [-0.8 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- ' [6.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 6.28] '
