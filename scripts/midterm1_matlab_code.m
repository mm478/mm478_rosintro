% the values for theta1, theta2, and theta3 for 
% each of the key poses I chose.
% theta 4,5,6 omitted because they are always 0 in my poses
pose1_theta_vals = [0, -3*pi/4, 3*pi/4]; 
pose2_theta_vals = [3*pi/4, -3*pi/4, 3*pi/4];
pose3_theta_vals = [pi/6, 0, 0];

A_pose1 = a_matrix(pose1_theta_vals)
A_pose2 = a_matrix(pose2_theta_vals)
A_pose3 = a_matrix(pose3_theta_vals)


J1 = jacobian_matrix(pose1_theta_vals)
J2 = jacobian_matrix(pose2_theta_vals)
J3 = jacobian_matrix(pose3_theta_vals)

%function that returns the H matrix given theta, d, a, alpha 
function H = h_matrix(theta, d, a, alpha)
    % declare variables for code readability 
    c_t = cos(theta);
    s_t = sin(theta);
    c_a = cos(alpha);
    s_a = sin(alpha);
    
    % first: rot_z_theta , transl_z_d, transl_x_a 
    first = [[c_t, -s_t, 0, a*c_t],
             [s_t,  c_t, 0, a*s_t],
             [  0,    0, 1,     d],
             [  0,    0, 0,     1]];

    % second: rot_x_alpha
    second = [[1,   0,    0, 0],
              [0, c_a, -s_a, 0],
              [0, s_a,  c_a, 0],
              [0,   0,    0, 1]];
    % multiply homogenous transformations together 
    H = first*second;
end

% function that takes the poses joint parameters, 
% calculates the H matrix for each joint, 
% and multiplies them together to return the 
% poses  overall A matrix
function A = a_matrix(pose_theta_vals)
    theta1 = pose_theta_vals(1);
    theta2 = pose_theta_vals(2);
    theta3 = pose_theta_vals(3);
    d1 = 0.1625;
    a2 = 0.425;
    a3 = 0.3922;
    d4 = 0.133;
    d5 = 0.0997;
    d6 = 0.0996;

    % see written portion for how I got these values of 
    % theta, d, a, and alpha for these joint matrices 
    h1 = h_matrix(theta1, d1, 0, pi/2);
    h2 = h_matrix(theta2 + pi/2, 0, a2, 0);
    h3 = h_matrix(theta3, 0, a3, 0);


    % these matrices are all the same for my poses 
    % because I didnt ever rotate the wrist
    H3_4 = h_matrix(0-pi/2, d4, 0, -pi/2); 
    H4_5 = h_matrix(0, d5, 0, -pi/2);
    H5_6 = h_matrix(0, d6, 0, 0);

    % multiply all H matrices together 
    % round to 2 places for readability 
    A = round(h1 * h2 * h3 * H3_4 * H4_5 * H5_6,2); 


end

function jacobian = jacobian_matrix(pose_theta_vals)
    theta1 = pose_theta_vals(1);
    theta2 = pose_theta_vals(2);
    theta3 = pose_theta_vals(3);
    d1 = 0.1625;
    a2 = 0.425;
    a3 = 0.3922;
    d4 = 0.1333;
    d5 = 0.0997;
    d6 = 0.0996;
    h1 = h_matrix(theta1, d1, 0, pi/2);
    h2 = h_matrix(theta2, 0, a2, 0);
    h3 = h_matrix(theta3, 0, a3, 0);
    H3_4 = h_matrix(0, d4, 0, pi/2); 
    H4_5 = h_matrix(0, d5, 0, -pi/2);
    H5_6 = h_matrix(0, d6, 0, 0);
    h0_2 = h1*h2;
    h0_3 = h1*h2*h3;
    h0_4 = h1*h2*h3*H3_4;
    h0_5 = h1*h2*h3*H3_4*H4_5;
    h0_6 = h1*h2*h3*H3_4*H4_5*H5_6;
    z0 = [0,0,1].;
    z1 = h1(1:3,3);
    z2 = h0_2(1:3,3);
    z3 = h0_3(1:3,3);
    z4 = h0_4(1:3,3);
    z5 = h0_5(1:3,3);
    o0 = [0,0,0];
    o1 = h1(1:3,4).;
    o2 = h0_2(1:3,4).;
    o3 = h0_3(1:3,4).;
    o4 = h0_4(1:3,4).;
    o5 = h0_5(1:3,4).;
    o6 = h0_6(1:3,4).;
    temp1 = horzcat( ...
        cross(z0, o6-o0)., ...
        cross(z1, o6-o1)., ...
        cross(z2, o6-o2)., ...
        cross(z3, o6-o3)., ...
        cross(z4, o6-o4)., ...
        cross(z5, o6-o5).);
    temp2 = horzcat(z0,z1,z2,z3,z4,z5);
    jacobian = round(vertcat(temp1,temp2),2);
end
