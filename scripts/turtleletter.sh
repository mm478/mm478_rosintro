#!/usr/bin/bash

rosservice call turtle1/set_pen 255 0 0 4 1

rosservice call turtle1/teleport_absolute 1.0 1.0 1.57

rosservice call turtle1/set_pen 255 0 0 4 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [3.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [4.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [0.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [4.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , -3.1415] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- ' [3.0 , 0.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
