from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import numpy as np

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import fabs, cos, sqrt
    tau = pi/2.0
    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

PI = np.pi - 0.01 # for more readability, but to prevent joints from getting to close to their max values 
beginning_frame = ((0,0,0,0,0,0),)
idle_frame = (0, -3*PI/4, 3*PI/4, 0, 0, 0) #KEY POSE 1
M_orientations = (
    ( 3*PI/4, -3*PI/4,   3*PI/4, 0, 0, 0), #starting position (KEY POSE 2)
    ( 3*PI/4,   -PI/5,   2*PI/5, 0, 0, 0), #A (bottom left)
    (   PI/2,   -PI/4,   2*PI/4, 0, 0, 0), #A->B (left)
    (   PI/3,   -PI/5,   2*PI/5, 0, 0, 0), #A->B (top/middle left)
    (   PI/6,       0,        0, 0, 0, 0), #B (top left) (KEY POSE 3)
    (      0,   -PI/3,   2*PI/3, 0, 0, 0), #C (middle)
    (  -PI/6,       0,        0, 0, 0, 0), #D (top right)
    (  -PI/3,   -PI/5,   2*PI/5, 0, 0, 0), #D->E (top/middle right)
    (  -PI/2,   -PI/4,   2*PI/4, 0, 0, 0), #D->E (right)
    (-3*PI/4,   -PI/5,   2*PI/5, 0, 0, 0), #E (bottom right)
    (-3*PI/4, -3*PI/4,   3*PI/4, 0, 0, 0), #ending 
)

I_orientations = (
    (      0,       0,        0, 0, 0, 0), #top 
    (      0,   -PI/3,   2*PI/3, 0, 0, 0), #bottom 

)



class MoveGroupDrawInitials(object):

    def __init__(self):
        super(MoveGroupDrawInitials, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_draw_initials", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"

        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

 
        planning_frame = move_group.get_planning_frame()
        eef_link = move_group.get_end_effector_link()
        group_names = robot.get_group_names()


        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, joint_vals): 
        # joint_vals is an array that contains the 
        # desired angle for each joint in the desired pose, in format:
        # (theta_1, theta_2, theta_3, theta_4, theta_5, theta_6)

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal = joint_vals

        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return 

    
    # takes in a set of poses, and the starting/ending orientation 
    # for the letter we want to draw 
    def draw(self, joint_orientations, idle_frame):
        # joint_vals is an array, where each entry is an array corresponding to 
        # a pose in the letter's trajectory. The nested array contains the 
        # desired angle for each joint in the desired pose. 
        self.go_to_joint_state(idle_frame) # pose for between letters 
        print("   orienting...")
        for idx, joint_vals in enumerate(joint_orientations):
            if(idx == 2): #if we've oriented and are at the position to begin drawing 
                print("   drawing... ") #notify the user that the pen is up 
            if(idx == len(joint_orientations)-1): #if we're at the end of the drawing 
                print("   re-orienting...") #notifying the user that the pen is up 
            self.go_to_joint_state(joint_vals)
        self.go_to_joint_state(idle_frame) # pose for between letters 
        return 


def main():
    try:
        input(
            "Press `Enter` to begin setup"
        )
        print("To properly view the drwaing, view from above")
        drawing = MoveGroupDrawInitials() #create an instance of a Drawing 
        

        drawing.go_to_joint_state(beginning_frame) #to re-orient the robot to its starting position 
        input(
            "Press `Enter` to begin drawing the M!"
        )  
        #drawing.draw(M_orientations, idle_frame)
        print("Hooray! We drew an M.. Marvelous ;) ")
        input(
            "Press `Enter` to begin drawing the I!"
        )  
        #drawing.draw(I_orientations, idle_frame)
        print("Hooray! We drew an I.. Incredible ;) ")
        input(
            "Press `Enter` to begin drawing another M!"
        ) 
        #drawing.draw(M_orientations, idle_frame)
        print("Hooray! We drew another M! Magnificent ;)")
        print("Done drawing initials M I M")


    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()